<?php


namespace polyv\src\channel;

/**
 * 直播频道创建
 * Class Create
 * @package polyv\src
 */
class Create extends BaseAbstract
{
	protected $url = 'https://api.polyv.net/live/v3/channel/basic/create';
}