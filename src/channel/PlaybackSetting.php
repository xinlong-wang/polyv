<?php


namespace polyv\src\channel;

use Illuminate\Support\Facades\Validator;
use polyv\src\InvalidArgumentException;

/**
 * 回放设置
 * Trait PlaybackSetting
 * @package polyv\src\channel
 */
trait PlaybackSetting
{
    protected $playback = [];

    /**
     * 是否应用通用设置 Y：是 N：否
     * @param $globalSettingEnabled
     */
    public function setGlobalSettingEnabled($globalSettingEnabled): void
    {
        $this->playback['globalSettingEnabled'] = $globalSettingEnabled;
    }

    /**
     * 回放开关 Y：开启 N：关闭
     * @param $playbackEnabled
     */
    public function setplaybackEnabled($playbackEnabled): void
    {
        $this->playback['playbackEnabled'] = $playbackEnabled;
    }

    /**
     * 回放方式 single：单个回放 list：列表回放
     * @param $type
     */
    public function setType($type): void
    {
        $this->playback['type'] = $type;
    }

    /**
     * 回放来源 record：暂存 playback：回放列表 vod：点播列表
     * @param $origin
     */
    public function setOrigin($origin): void
    {
        $this->playback['origin'] = $origin;
    }

    /**
     * 单个回放的视频id
     * @param $videoId
     */
    public function setVideoId($videoId): void
    {
        $this->playback['videoId'] = $videoId;
    }


    public function playBackCheck(): void
    {
        $rules = [
            'globalSettingEnabled' => ['string', 'in:Y,N'],
            'playbackEnabled' => ['string', 'in:Y,N'],
            'type' => ['string', 'in:single,list'],
            'origin' => ['string', 'in:record,playback,vod'],
            'videoId' => ['string']
        ];
        $validator = Validator::make($this->playback, $rules);
        $error = $validator->errors()->first();
        if ($error) {
            throw new InvalidArgumentException($error);
        }
    }
}