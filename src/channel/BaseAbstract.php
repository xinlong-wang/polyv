<?php

namespace polyv\src\channel;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Log;
use polyv\src\Basic;

abstract class BaseAbstract implements BaseInterface
{
	protected $data = [];
	protected $url;
	use BasicSettings;
	use AuthSettings;
	use PlaybackSetting;
	use TeacherSettings;
	use RolesSettings;

	final public function buildData(): void
	{
		if (!empty($this->basicSetting)) {
			$this->basicCheck();
			$this->data['basicSetting'] = $this->basicSetting;
		}
		if (!empty($this->auth)) {
			$this->authCheck();
			$this->data['authSettings'] = [$this->auth];
		}
		if (!empty($this->teacher)) {
			$this->teacherCheck();
			$this->data['teacher'] = $this->teacher;
		}
		if (!empty($this->playback)) {
			$this->playBackCheck();
			$this->data['playbackSetting'] = $this->playback;
		}
		if (!empty($this->role)) {
			$this->rolesCheck();
			$this->data['roles'] = $this->role;
		}
	}

	/**
	 * @param array $params
	 */
	public function buildUrl(array $params = []): void
	{
		$appid = env('POLYV_APP_ID');
		$timestamp = Basic::getTimestamp();
		$params['appId'] = $appid;
		$params['timestamp'] = $timestamp;
		$params['sign'] = Basic::getSign($params);
		$this->url .= '?' . http_build_query($params);
	}

	/**
	 * 发送创建/修改频道请求
	 * @return string
	 * @throws GuzzleException
	 * @throws \JsonException
	 */
	public function send(): string
	{
		$this->buildData();
		$request = new Request('POST', $this->url, ['Content-Type' => 'application/json'], json_encode($this->data, JSON_THROW_ON_ERROR));
		$client = new Client();
		$contents = $client->send($request, ['http_errors' => false])->getBody()->getContents();
		Log::debug($contents);
		return $contents;
	}
}
