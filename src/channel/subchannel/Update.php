<?php


namespace polyv\src\channel\subchannel;


use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Log;

/**
 * 修改子频道信息
 * Class Update
 * @package polyv\src\channel\subchannel
 */
class Update extends SubChannel
{

    /**
     * 子频道号（不能以数字类型提交，否则可能去掉子频道号前的00）
     * @param string $account
     */
    public function setAccount(string $account): void
    {
        $this->params['account'] = $account;
    }

    /**
     * 子频道头衔
     * @param string $actor
     */
    public function setActor(string $actor): void
    {
        $this->params['actor'] = $actor;
    }

    /**
     * 子频道头像
     * @param string $avatar
     */
    public function setAvatar(string $avatar): void
    {
        $this->params['avatar'] = $avatar;
    }


    /**
     * 子频道昵称
     * @param string $nickname
     */
    public function setNickname(string $nickname): void
    {
        $this->params['nickname'] = $nickname;
    }

    /**
     * @param string $role
     */
    public function setRole(string $role): void
    {
        $this->params['role'] = $role;
    }

    /**
     * 设置子频道密码
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->params['password'] = $password;
    }

    /**
     * 子频道翻页权限，只能一个子频道有，仅支持三分屏场景 Y：开启 N：关闭
     * @param string $pageTurnEnabled
     */
    public function setPageTurnEnabled(string $pageTurnEnabled): void
    {
        $this->params['pageTurnEnabled'] = $pageTurnEnabled;
    }

    /**
     * 子频道公告权限 Y：开启 N：关闭
     * @param string $notifyEnabled
     */
    public function setNotifyEnabled(string $notifyEnabled): void
    {
        $this->params['notifyEnabled'] = $notifyEnabled;
    }

    public function send()
    {
        parent::send();
        if (empty($this->channelId)) {
            throw new \InvalidArgumentException('频道ID不能为空');
        }
        if (empty($this->params['account'])) {
            throw new \InvalidArgumentException('子频道号不能为空');
        }
        $url = 'https://api.polyv.net/live/v2/channelAccount/%s/update?';
        $request = new Request('POST', sprintf($url, $this->channelId) . http_build_query($this->params));
        $client = new Client();
        $content = $client->send($request, ['http_errors' => false])->getBody()->getContents();
        Log::debug('修改子频道：' . $content);
        return $content;
    }
}