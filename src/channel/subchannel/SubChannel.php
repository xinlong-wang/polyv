<?php


namespace polyv\src\channel\subchannel;


use polyv\src\Basic;

abstract class SubChannel extends Basic
{
    protected $channelId;

    /**
     * 设置频道ID
     * @param int $channel_id
     */
    public function setChannelId(int $channel_id): void
    {
        $this->channelId = $channel_id;
    }
}