<?php

namespace polyv\src\channel;

interface BaseInterface
{
	public function buildData();
	public function buildUrl();
	public function send();
}